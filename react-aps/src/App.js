import axios from "axios";
import { useEffect, useState } from "react";
import style from "./app.module.scss";
import Topic from "./component/Topic";
import Grafic from "./component/Grafic";
import CardCurs from "./component/CardCurs";

function App() {
  const [dataRespons, setDataRespons] = useState("");
  const [prognpzEgz, setPrognozEgz] = useState(0);
  const [searchValue, setSearchValue] = useState("");
  const [stateArrLessonScore, setStateArrLessonScore] = useState(0);

  let arrLessonScore = [];

  const updateData = (id, value1) => {
    const data = {
      id: id,
      value1: value1,
    };

    axios
      .post("http://aps/connect_bd.php", data)
      .then((response) => {
        // Обработка успешного ответа от сервера
        console.log(response.data);
      })
      .catch((error) => {
        // Обработка ошибки
        console.error(error);
      });
  };
  const onWriteScore = (el) => {
    // console.log("elem", el);
    console.log("el", el.target.value, "id", el.target.name);
    arrLessonScore[el.target.name] = el.target.value;
    // setStateArrLessonScore(arrLessonScore);
    updateData(el.target.name, el.target.value);
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get("http://aps/connect_bd.php");
        const data = response.data;
        setDataRespons(data); // Обработка полученных данных
      } catch (error) {
        console.log(error);
      }
    };

    // Функция для отправки SQL запроса на сервер

    // updateData(1, 5);

    fetchData();
  }, arrLessonScore);

  console.log(dataRespons);
  console.log(dataRespons["card_lesson"]);

  if (dataRespons["card_lesson"]) {
    for (let i = 0; i < dataRespons["card_lesson"].length; i++) {
      arrLessonScore.push(dataRespons["card_lesson"][i]["score"]);
    }
    console.log("arrLessonScore", arrLessonScore);
    // setStateArrLessonScore(arrLessonScore);
  }

  return (
    <div className={style.app}>
      {dataRespons ? (
        <div className={style.wrapper}>
          <div className={style.user}>
            <img src="./img/user.png" alt="user"></img>
            <p>
              {dataRespons["user"][0]["surname"] +
                " " +
                dataRespons["user"][0]["name"]}
            </p>
          </div>
          <h2>Оценки</h2>

          <div className={style.table}>
            <Topic
              onWriteScore={onWriteScore}
              tip={"З"}
              tema={"Тема 1"}
              cardLesson={dataRespons["card_lesson_t1"]}
              score={dataRespons["card_topic"][0]["score"]}
            />
            <Topic
              onWriteScore={onWriteScore}
              tip={"З"}
              tema={"Тема 2"}
              cardLesson={dataRespons["card_lesson_t2"]}
              score={dataRespons["card_topic"][1]["score"]}
            />
            <Topic
              onWriteScore={onWriteScore}
              tip={"З"}
              tema={"Тема 3"}
              cardLesson={dataRespons["card_lesson_t3"]}
              score={dataRespons["card_topic"][2]["score"]}
            />
            <Topic
              onWriteScore={onWriteScore}
              tip={"З"}
              tema={"Тема 4"}
              cardLesson={dataRespons["card_lesson_t4"]}
              score={"нет оценки"}
            />

            <h3>
              Оценка за экзамен{" "}
              <input
                style={{ width: "100px" }}
                className={style.intput_score}
                placeholder="нет оценки"
              ></input>
              {/* {dataRespons["card_object"][0]["score"]}{" "} */}
            </h3>
          </div>
          <h3 className={style.h3_graf}>
            Прогноз на экзамен
            <span style={{ opacity: "50%" }}>
              {" "}
              : {dataRespons["graf"][53]["grade"]}
            </span>
          </h3>

          <div className={style.grafic}>
            <Grafic
              graf={dataRespons}
              flag1={dataRespons["card_lesson_t4"]}
              cardLesson={dataRespons["card_lesson"]}
              stateArrLessonScore={arrLessonScore}
            />
          </div>

          <div>
            <CardCurs topic={dataRespons["topics_to_study"]} />
            <br></br>
            <br></br>
          </div>
        </div>
      ) : (
        "Loading..."
      )}
    </div>
  );
}

export default App;
