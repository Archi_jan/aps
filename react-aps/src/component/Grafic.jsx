import React, { useEffect, useState } from "react";
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
} from "recharts";
import { linearRegression } from "simple-statistics";

const Grafic = (graf) => {
  ////////////

  const predictFutureGrades = (grades) => {
    const startDate = new Date(grades[0].date);
    const xValues = grades.map(({ date }) => {
      const currentDate = new Date(date);
      const daysPassed = Math.floor(
        (currentDate - startDate) / (1000 * 60 * 60 * 24)
      );
      return daysPassed;
    });
    const yValues = grades.map(({ grade }) => grade);
    const regression = linearRegression(xValues, yValues);

    const futureGrades = [];
    for (
      let i = xValues[xValues.length - 1] + 1;
      i < xValues[xValues.length - 1];
      i++
    ) {
      const predictedGrade = regression[0] + regression[1] * i;
      const futureDate = new Date(startDate);
      futureDate.setDate(startDate.getDate() + i);
      const formattedDate = futureDate.toLocaleDateString("en-US");

      futureGrades.push({
        date: formattedDate,
        grade: Number(predictedGrade.toFixed(2)),
      });
    }

    return futureGrades;
  };

  // console.log("===", graf.flag1[graf.flag1.length - 1].id);
  ////////////
  console.log("score ", graf.stateArrLessonScore);
  const [grNes, setGr] = useState([]);
  let gr = [];
  const len = graf.flag1[graf.flag1.length - 1].id - graf.flag1.length;
  for (let i = 0; i < graf["graf"]["graf"].length; i++) {
    if (i <= len - 10) {
      gr.push({
        date: (i + 7) * 2,
        current: graf.stateArrLessonScore[i],
      });
    }
    if (i >= len) {
      gr.push({
        date: (i + 7) * 2,
        forecast: graf["graf"]["graf"][i - len].grade,
      });
    }
  }
  console.log("gr", gr);

  const [studentGrades, setStudentGrades] = useState(gr);
  const predictedGrades = predictFutureGrades(studentGrades);

  return (
    <LineChart
      width={800}
      height={400}
      data={studentGrades.concat(predictedGrades)}
    >
      <CartesianGrid strokeDasharray="3 3" />
      <XAxis dataKey="date" />
      <YAxis />
      <Tooltip />
      <Legend />
      <Line
        type="monotone"
        dataKey="current"
        stroke="#8884d8"
        strokeWidth={2}
      />
      <Line
        type="monotone"
        dataKey="forecast"
        stroke="#DF3636"
        strokeWidth={2}
      />
    </LineChart>
  );
};

export default Grafic;
