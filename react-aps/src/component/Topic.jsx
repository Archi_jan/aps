import style from "./../app.module.scss";

function Topic(props) {
  return (
    <div className={style.topic}>
      <h3>{props.tema}</h3>
      <div className={style.table_inner}>
        <table>
          <thead>
            <tr>
              {props.cardLesson.map((el, index) => (
                <th>
                  {props.tip}-{index + 1}
                </th>
              ))}

              {/* {dataRespons["card_topic"].map((el, index) => (
                      <th>КР-{index + 1}</th>
                    ))} */}
            </tr>
          </thead>
          <tbody>
            <tr>
              {props.cardLesson.map((el) => (
                <td>
                  <input
                    onChange={props.onWriteScore}
                    name={el["id"]}
                    className={style.intput_score}
                    placeholder={el["score"]}
                  ></input>
                </td>
              ))}
              {/* {dataRespons["card_topic"].map((el) => (
                      <th>{el["score"]}</th>
                    ))} */}
            </tr>
          </tbody>
        </table>
      </div>
      <div className={style.score}>
        Итоговое КМ :{" "}
        <input
          style={{ width: "100px" }}
          className={style.intput_score}
          placeholder={props.score}
        ></input>{" "}
      </div>
    </div>
  );
}
export default Topic;
