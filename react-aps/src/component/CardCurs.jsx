import React from "react";
import style from "./../app.module.scss";

function CardCurs(props) {
  return (
    <div className={style.cardcurs}>
      <h3>Курсы к изучению</h3>
      <div className={style.cardcurs_inner}>
        <h4>{props.topic[0]["name"]}</h4>
        <p>
          Освоите новую профессию с нуля и начнете помогать бизнесу, принимая
          ключевые решения на основе данных
        </p>
        <a href="#">Узнать подробнее</a>
      </div>
    </div>
  );
}
export default CardCurs;
