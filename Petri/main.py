from datetime import date
import time
import json
import mysql.connector
import itertools

 # Подключение к базе данных
cnx = mysql.connector.connect(user='root', password='',host='localhost', database='APS')
cursor = cnx.cursor()
query = "SELECT score FROM card_topic"
cursor.execute(query)
data = cursor.fetchall()
cnx.close()
arr = [int(str(num[0])) for num in data]
print(arr)

all_tasks = []

class task:
    def __init__(self, name, id, deadline = 365, requirements = []):
        self.is_completed = False # по умолчанию считаем, что таска актуальная
        self.name = name # название таски
        self.deadline = date.fromtimestamp(time.time() + deadline*60*60*24)
        self.requirements = requirements # требования
        self.listeners = []
        self.doable = (requirements == [])
        self.id = id
        for req in requirements:
            get_task_by_id(req).add_listener(self)
        all_tasks.append(self)


    def add_listener(self, task):
        self.listeners.append(task)

    #отметить задачу, как выполненную
    def complete_task(self):
        self.is_completed = True
        for listener in self.listeners:
            listener.requirements.remove(self.id)
            print("del", self.id)
            if (listener.requirements == []):
                listener.doable = True

# задача по айдишнику
def get_task_by_id(id):
    for task in all_tasks:
        if task.id == id:
            return task

# загрузка из файла
# json файл будет содержать список тем, необходимых для изучения
# и порядок, в котором они должны быть изучены
def load_task_from_json(data):
    month, day = str.split(data["deadline"])
    deadline = (int(month)-int(date.today().strftime("%m")))*31 + (int(day)-int(date.today().strftime("%d")))
    return task(data["name"], data["id"], deadline=deadline, requirements = data["requirements"])


#собираем сеть Петри.
# заменить дублирование на цикл
# в принципе, можно сразу возвращать сразу список тасков
def create_test_graph():

    file = open("D:/Protage/My Project File/aps-main/Petri/tasks.json")
    tasks = json.load(file)

    task1 = load_task_from_json(tasks[0])
    task2 = load_task_from_json(tasks[1])
    task3 = load_task_from_json(tasks[2])
    task4 = load_task_from_json(tasks[3])
    task5 = load_task_from_json(tasks[4]) # экзамен
    
    if arr[0] >=3:
        task1.complete_task() #для примера изменим статус задачи тут
        print("task1 done")   
    if arr[1]>=3:
        task2.complete_task()
        print("task2 done")
    if arr[2]>=3:
        task3.complete_task()
        print("task3 done")
    if arr[3]>=3:
        task4.complete_task()
        print("task4 done")
    return [task1, task2, task3, task4, task5] #временный костыль



#принт по полям
def print_graph(graph) :
    for elem in graph:
        print("MyTask " + elem.name + " : todo until "
              + elem.deadline.strftime("%m")
              + " " + elem.deadline.strftime("%d")
              + ", Realy? : " + str(elem.doable)
              + ", Not empty if false = ", elem.requirements)
    mass = []    
    for elem in graph:
        if elem.requirements not in(mass):
            mass.append(elem.requirements)
    return mass



#                MAIN

scenario = []

if __name__ == '__main__':
    graph = create_test_graph()
    scenario = print_graph(graph)
    scenario = list(itertools.chain.from_iterable(scenario))
    scenario = list(set(scenario))


def my_topic(scenario):
    arr_tems = []
    print(scenario)
     # Подключение к базе данных
    cnx = mysql.connector.connect(user='root', password='',host='localhost', database='APS')
    cursor = cnx.cursor()
    for i in range(1, len(scenario)+1):
        query = "SELECT name FROM topic WHERE id={}".format(scenario[i-1])
        cursor.execute(query)
        data = cursor.fetchall()
        arr_tems.append([(str(num[0])) for num in data])
    print(arr_tems)
    cursor.execute('DELETE FROM topics_to_study')
    cnx.commit()
    for el in scenario:
        cursor.execute('INSERT INTO topics_to_study (id_user, id_topic) VALUES (%s, %s)', (1, el))
    # Подтвердить выполнение транзакции
    cnx.commit()
    cnx.close()


my_topic(scenario)