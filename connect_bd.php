<?php
    // Разрешение POST запросов
header("Access-Control-Allow-Origin: http://localhost:3000");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type"); 
$servername = "localhost";   // Замените localhost на адрес вашего сервера
$username = "root";   // Замените имя_пользователя на ваше
$password = "";   // Замените пароль на ваш
$dbname = "APS";   // Замените имя_базы_данных на ваше

$conn = mysqli_connect($servername, $username, $password, $dbname);

if (!$conn) {
    die("Ошибка подключения к базе данных: " . mysqli_connect_error());
}


$sql = "SELECT * FROM user";
$result = mysqli_query($conn, $sql);
// Создаем массив для хранения данных
$data = array();
    $data['user'] = [];
if (mysqli_num_rows($result) > 0) {
    // Добавляем данные из каждой строки в массив
    while ($row = mysqli_fetch_assoc($result)) {
        // $row = array_map('utf8_encode', $row);
        $data['user'][] = $row;
    }
    

    // Преобразовываем массив данных в JSON-строку
    $json = json_encode($data, JSON_UNESCAPED_UNICODE);

    // Выводим JSON-строку
    // echo $json;
} else {
    echo "Нет данных в таблице";
}


$sql = "SELECT * FROM object";
$result = mysqli_query($conn, $sql);
// Создаем массив для хранения данных
// $data = array();
if (mysqli_num_rows($result) > 0) {
    $data['object'] = [];
    // Добавляем данные из каждой строки в массив
    while ($row = mysqli_fetch_assoc($result)) {
        // $row = array_map('utf8_encode', $row);
        $data['object'][] = $row;
    }

    // Преобразовываем массив данных в JSON-строку
    $json = json_encode($data, JSON_UNESCAPED_UNICODE);

    // Выводим JSON-строку
    // echo $json;
} else {
    echo "Нет данных в таблице";
}



$sql = "SELECT * FROM topic";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    $data['topic'] = [];
    // Добавляем данные из каждой строки в массив
    while ($row = mysqli_fetch_assoc($result)) {
        // $row = array_map('utf8_encode', $row);
        $data['topic'][] = $row;
    }

    // Преобразовываем массив данных в JSON-строку
    $json = json_encode($data, JSON_UNESCAPED_UNICODE);

    // Выводим JSON-строку
} else {
    echo "Нет данных в таблице";
}



$sql = "SELECT * FROM lesson";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    $data['lesson'] = [];
    // Добавляем данные из каждой строки в массив
    while ($row = mysqli_fetch_assoc($result)) {
        // $row = array_map('utf8_encode', $row);
        $data['lesson'][] = $row;
    }

    // Преобразовываем массив данных в JSON-строку
    $json = json_encode($data, JSON_UNESCAPED_UNICODE);

    // Выводим JSON-строку
} else {
    echo "Нет данных в таблице";
}

$sql = "SELECT * FROM card_object";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    $data['card_object'] = [];
    // Добавляем данные из каждой строки в массив
    while ($row = mysqli_fetch_assoc($result)) {
        // $row = array_map('utf8_encode', $row);
        $data['card_object'][] = $row;
    }

    // Преобразовываем массив данных в JSON-строку
    $json = json_encode($data, JSON_UNESCAPED_UNICODE);

    // Выводим JSON-строку
} else {
    echo "Нет данных в таблице";
}


$sql = "SELECT * FROM card_topic";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    $data['card_topic'] = [];
    // Добавляем данные из каждой строки в массив
    while ($row = mysqli_fetch_assoc($result)) {
        // $row = array_map('utf8_encode', $row);
        $data['card_topic'][] = $row;
    }

    // Преобразовываем массив данных в JSON-строку
    $json = json_encode($data, JSON_UNESCAPED_UNICODE);

    // Выводим JSON-строку
} else {
    echo "Нет данных в таблице";
}

$sql = "SELECT * FROM card_lesson";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    $data['card_lesson'] = [];
    // Добавляем данные из каждой строки в массив
    while ($row = mysqli_fetch_assoc($result)) {
        // $row = array_map('utf8_encode', $row);
        $data['card_lesson'][] = $row;
    }

    // Преобразовываем массив данных в JSON-строку
    $json = json_encode($data, JSON_UNESCAPED_UNICODE);

    // Выводим JSON-строку
} else {
    echo "Нет данных в таблице";
}


$sql = "SELECT c.id, c.id_user, c.id_lesson, c.score, l.name, l.id_topic
FROM card_lesson c
JOIN lesson l ON c.id_lesson = l.id
WHERE l.id_topic = 1;";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    $data['card_lesson_t1'] = [];
    // Добавляем данные из каждой строки в массив
    while ($row = mysqli_fetch_assoc($result)) {
        // $row = array_map('utf8_encode', $row);
        $data['card_lesson_t1'][] = $row;
    }

    // Преобразовываем массив данных в JSON-строку
    $json = json_encode($data, JSON_UNESCAPED_UNICODE);

    // Выводим JSON-строку
    // echo $json;
} else {
    echo "Нет данных в таблице";
}


$sql = "SELECT c.id, c.id_user, c.id_lesson, c.score, l.name, l.id_topic
FROM card_lesson c
JOIN lesson l ON c.id_lesson = l.id
WHERE l.id_topic = 2;";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    $data['card_lesson_t2'] = [];
    // Добавляем данные из каждой строки в массив
    while ($row = mysqli_fetch_assoc($result)) {
        // $row = array_map('utf8_encode', $row);
        $data['card_lesson_t2'][] = $row;
    }

    // Преобразовываем массив данных в JSON-строку
    $json = json_encode($data, JSON_UNESCAPED_UNICODE);

    // Выводим JSON-строку
    // echo $json;
} else {
    echo "Нет данных в таблице";
}


$sql = "SELECT c.id, c.id_user, c.id_lesson, c.score, l.name, l.id_topic
FROM card_lesson c
JOIN lesson l ON c.id_lesson = l.id
WHERE l.id_topic = 3;";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    $data['card_lesson_t3'] = [];
    // Добавляем данные из каждой строки в массив
    while ($row = mysqli_fetch_assoc($result)) {
        // $row = array_map('utf8_encode', $row);
        $data['card_lesson_t3'][] = $row;
    }

    // Преобразовываем массив данных в JSON-строку
    $json = json_encode($data, JSON_UNESCAPED_UNICODE);

    // Выводим JSON-строку
    // echo $json;
} else {
    echo "Нет данных в таблице";
}


$sql = "SELECT c.id, c.id_user, c.id_lesson, c.score, l.name, l.id_topic
FROM card_lesson c
JOIN lesson l ON c.id_lesson = l.id
WHERE l.id_topic = 4;";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    $data['card_lesson_t4'] = [];
    // Добавляем данные из каждой строки в массив
    while ($row = mysqli_fetch_assoc($result)) {
        // $row = array_map('utf8_encode', $row);
        $data['card_lesson_t4'][] = $row;
    }

    // Преобразовываем массив данных в JSON-строку
    $json = json_encode($data, JSON_UNESCAPED_UNICODE);

    // Выводим JSON-строку
} else {
    echo "Нет данных в таблице";
}


$sql = "SELECT * FROM graf";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    $data['graf'] = [];
    // Добавляем данные из каждой строки в массив
    while ($row = mysqli_fetch_assoc($result)) {
        // $row = array_map('utf8_encode', $row);
        $data['graf'][] = $row;
    }

    // Преобразовываем массив данных в JSON-строку
    $json = json_encode($data, JSON_UNESCAPED_UNICODE);

    // Выводим JSON-строку

} else {
    echo "Нет данных в таблице";
}


$sql = "SELECT *
FROM topics_to_study
JOIN topic ON topics_to_study.id_topic = topic.id;";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    $data['topics_to_study'] = [];
    // Добавляем данные из каждой строки в массив
    while ($row = mysqli_fetch_assoc($result)) {
        // $row = array_map('utf8_encode', $row);
        $data['topics_to_study'][] = $row;
    }

    // Преобразовываем массив данных в JSON-строку
    $json = json_encode($data, JSON_UNESCAPED_UNICODE);

    // Выводим JSON-строку
    echo $json;
} else {
    echo "Нет данных в таблице";
}



// Получение данных из POST запроса
$data = json_decode(file_get_contents('php://input'), true);

// Изменение данных в таблице
$sql = "UPDATE card_lesson SET score = '{$data['value1']}' WHERE id_lesson = '{$data['id']}'";
$conn->query($sql)

// Закрываем соединение с базой данных
// mysqli_close($conn);

?>