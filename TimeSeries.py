import mysql.connector
from datetime import date, timedelta
import pandas as pd
import numpy as np
from statsmodels.tsa.arima.model import ARIMA
import matplotlib.pyplot as plt


# Подключение к базе данных
cnx = mysql.connector.connect(user='root', password='',host='localhost', database='APS')
cursor = cnx.cursor()
query = "SELECT score FROM card_lesson"
cursor.execute(query)
data = cursor.fetchall()
# Закрытие соединения
cursor.close()
cnx.close()

# Вывод результатов
mass=[]
arr = [int(str(num[0])) for num in data]
print(arr)


# Задаем даты прогнозирования
start_date = date.today()
end_date = start_date + timedelta(days=len(arr)-1)  

# Создаем пустой список для хранения дат
dates_list = []

# Используем цикл while для добавления последовательных дат в список
current_date = start_date
while current_date <= end_date:
    dates_list.append(current_date)  
    current_date += timedelta(days=1)

# Выводим список дат
for date in dates_list:
    print(date)


# Загрузка данных с оценками за тесты
data ={
    "date": dates_list,
    "grades": arr
}

train = data['grades'][:-20]
print("train", train)
test = data['grades'][-20:]
print("test",test)


# Обучение модели ARIMA на тренировочных данных и прогноз
model = ARIMA(train, order=(2, 1,4)) 
model_fit = model.fit()
forecast = model_fit.forecast(steps=len(arr))

# Вывод прогнозной оценки за экзамен
print('forecast',forecast)

# Распаковка данных
dates = data["date"]
grades = data["grades"]

# Создание графика
plt.plot(dates, grades, marker='o')
plt.plot(dates_list, forecast, marker='o', color='red', label='Прогноз')
plt.title("Оценки за тесты")
plt.xlabel("Дата")
plt.ylabel("Оценка")
plt.xticks(rotation=45)
plt.tight_layout()
plt.legend()
plt.show()


# Подключение к базе данных
cnx = mysql.connector.connect(user='root', password='',host='localhost', database='APS')
cursor = cnx.cursor()
ff = 0
cursor.execute('DELETE FROM graf')
cnx.commit()
for date in dates_list:
    cursor.execute('INSERT INTO graf (date, grade) VALUES (%s, %s)', (date, float(forecast[ff])))
    ff = ff+1
# Подтвердить выполнение транзакции
cnx.commit()
cnx.close()





