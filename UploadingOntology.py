from owlready2 import *
import sys
import io
import requests
import os
# Задайте URL-адрес Mokapi и путь к ресурсу для записи JSON
url = 'https://64baee415e0670a501d6bfa2.mockapi.io/list'

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')
ontology = get_ontology('.\\project.owl').load()

# Выведите все классы в онтологии
for cls in ontology.classes():
    onto = [{}]  # Создание пустого списка

    # print("Класс:", cls)
    onto[0]["Класс"] =  str(cls)[8:]
    # Выведите все свойства класса
    i = 1
    for prop in cls.is_a:
        # print("  Свойство:", str(prop))
        onto[0]["Свойство_"+str(i)] =  str(prop)[8:]
        i = i+1

    # Проверьте, есть ли экземпляры класса
    if cls.instances():
        # Выведите все экземпляры класса
        j = 1
        for instance in cls.instances():
            # print("  Экземпляр:", str(instance))
            onto[0]["Экземпляр_" +str(j)] =  str(instance)[8:]
            j = j+1

    # print("\n")

    # print("onto ",onto )
    

    # Отправляем запрос на удаление данных
    # for i in range(1,13):
    #     response = requests.delete('https://64baee415e0670a501d6bfa2.mockapi.io/list/{i}')

    response = requests.post(url, data=onto[0])
        








